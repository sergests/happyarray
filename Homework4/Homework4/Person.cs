﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework4
{
    internal class Person
    {
        private int age;
        public int Age 
        {
            get {return age;}
            set {age = value;}
        }
        public void Greet()
        {
            Console.WriteLine("Hello");
        }
        public int SetAge(int n)
        {
            return age;
        }

    }
}
