﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Directory
{
    internal class Request
    {
        public Request(int requestId, string licenseType,
        int status, int facilityId, DateTime dateCreated)
        {
            RequestId = requestId;
            LicenseType = licenseType;
            Status = status;
            FacilityId = facilityId;
            DateCreated = dateCreated;
        }
        public int RequestId { get; set; }
        public string LicenseType { get; set; }
        public int Status { get; set; }
        public int FacilityId { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
