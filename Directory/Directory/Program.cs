﻿using Directory;
using System.IO;
using System.Collections.Generic;


class Program
{
    static void Main(string[] args)
    {
        var filePath = @"C:\10_c#_Requests.txt.txt";
        var requestList = new List<Request>();

        using (var file = new StreamReader(filePath))
        {
            string? line;
            var counter = 1;
            while ((line = file.ReadLine()) != null)
            {
                if (counter == 1)
                {
                    counter++;
                    continue;
                }

                var request = Map(line);
                requestList.Add(request);

            }
        }
        //if (File.Exists(filePath))
        //{
        //    string text = File.ReadAllText(filePath);
        //    Console.WriteLine(text);
        //}
        
        var uniqueLicenses = requestList.GroupBy(c => c.LicenseType)
                                        .Select(c => c.Key).ToArray();
        foreach (var item in uniqueLicenses)
        {
            Console.WriteLine(item);
        }

        Console.WriteLine(new string('*', 35));
        var countByType = requestList.GroupBy(p => p.LicenseType)
                                     .ToDictionary(p => p.Key, p => p.Count());
        foreach (var item in countByType)
        {
            Console.WriteLine(item);
        }

        Console.WriteLine(new string('*', 35));
        var status2In2021 = requestList.GroupBy(p => p.Status);
        foreach (var item in status2In2021)
        {
            Console.WriteLine(item.Key);
            Console.WriteLine(String.Join(", ", item.Select(v => v.LicenseType)));
        }
        
        Console.WriteLine(new string('*', 35));
        var status2In2022 = requestList.GroupBy(p => p.DateCreated.Year == 2021 && p.Status == 2)
                                       
                                       .FirstOrDefault();
        foreach (var item in status2In2021)
        {
            if (true)
            {
                //Console.WriteLine($"{status2In2021.");
            }
            
        }
    }

    static Request Map(string line)
    {
        var lineArray = line.Split(",");

        return new Request(
            int.Parse(lineArray[0]),
            lineArray[1],
            int.Parse(lineArray[2]),
            int.Parse(lineArray[3]),
            DateTime.Parse(lineArray[4])
            );
    }
}

