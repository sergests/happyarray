﻿using System;

public class Car
{
	public Car()
	{
		public string Name;
		public string Description;
		public string Color; 
		
		public void Print()
		{
		Console.WriteLine($" Name: {Name}, Description: {Description}, Color: {Color}.")
		}
	}
}
