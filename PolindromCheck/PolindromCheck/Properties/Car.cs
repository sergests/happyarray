﻿using System;

public class Car
{
	
		public string Name;
		public string Description;
		public string Color; 
		
		public Car(string name, string color) {Name = name; Color = color;}
		public void Print()
		{
		Console.WriteLine($" Name: {Name}, Description: {Description}, Color: {Color}.");
		}
	
}
