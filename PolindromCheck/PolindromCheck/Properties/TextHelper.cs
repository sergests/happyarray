﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolindromCheck.Properties
{
    internal class TextHelper
    {
        int GetWordsCount(string text)
        {
            int wordCount = 0, index = 0;

            // skip whitespace until first word
            while (index < text.Length && char.IsWhiteSpace(text[index]))
                index++;

            while (index < text.Length)
            {
                // check if current char is part of a word
                while (index < text.Length && !char.IsWhiteSpace(text[index]))
                    index++;

                wordCount++;

                // skip whitespace until next word
                while (index < text.Length && char.IsWhiteSpace(text[index]))
                    index++;
            }
            return wordCount;

            int GetLettersCount(string text2)
            {
                int counter = 0;
                for (int i = 0; i < text2.Length; i++)
                {
                    counter++;
                }
                return counter;
            }

            string GetPolindroms(string word)
            {
                string reversed = string.Empty;
                for (int i = 0; i < word.Length - 1; i++)
                {
                    reversed += word[i];
                }
                if (reversed == word)
                {
                    return $" '{word}' is a POLINDROM";
                }
                else
                {
                    return $" '{word}' is NOT a Polindrom";
                }
                
            }

            int GetCountSpecificWords(string YourText, string specificWord)
            {
                string word = specificWord;
                string receivedText = YourText;
                //Substring with desired length
                receivedText = receivedText.Substring(0, length);
                int count = 0;
                //creating an array of words
                string[] words = receivedText.Split(Convert.ToChar(" "));
                //linq query
                count = words.Where(x => x == word).Count();
                Console.WriteLine(count);
            }
        }
    }
}
