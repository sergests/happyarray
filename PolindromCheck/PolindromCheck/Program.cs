﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolindromCheck
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please Enter a String : \n");
            string enteredWord = Console.ReadLine();

            string reversed = string.Empty;
            for (int i = 0; i < enteredWord.Length - 1; i++)
            {
                reversed += enteredWord[i];
            }
            if (reversed == enteredWord)
            {
                Console.WriteLine($" '{enteredWord}' is a POLINDROM");
            }
            else
            {
                Console.WriteLine($" '{enteredWord}' is NOT a Polindrom");
            }
            Console.ReadLine();


            /*var volvo = new Car("volvo", "silver");
            //volvo.Name = "Volvo";
            volvo.Description = "Good";
            //volvo.Color = "silver";

            volvo.Print();
            Console.ReadLine();
            

            Console.Write("Please Enter a String : \n");
            while (true)
            {
                string originalString = Console.ReadLine();
                string reverseString = string.Empty;
                for (int i = originalString.Length - 1; i >= 0; i--)
                {
                    reverseString += originalString[i];
                }
                if (originalString == reverseString)
                {
                    Console.WriteLine($"{originalString}: is a POLINDROM");
                }
                else
                {
                    Console.WriteLine($"{originalString}: is NOT a Polindrom");
                }
                Console.ReadLine();
            }
             */
        }

    }
}
