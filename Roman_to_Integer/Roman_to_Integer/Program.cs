﻿using Roman_to_Integer;

Translator translator = new Translator();


Console.WriteLine("Please Enter Your ROMAN NUMBER: \n");
string roman = Console.ReadLine();

int number = translator.RomanToInt(roman);
if (number == 0)
{
    Console.WriteLine("Entered NOT VALID NUMBER");
}
else
Console.WriteLine($"\n Your integer number is: {number}");
