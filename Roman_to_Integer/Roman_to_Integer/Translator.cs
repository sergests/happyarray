﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roman_to_Integer
{
    internal class Translator
    {
        public int RomanToInt(string romanTxt)
        {
            var arr = romanTxt.ToCharArray();
            char rChar = ' ';
            int result = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                switch (arr[i])
                {
                    case 'I':
                        if (result == 3)
                        {
                            result = 0;
                            break;
                        }
                        else
                        {
                            result += 1;
                            break;
                        }
                    case 'V':
                        {
                            if (rChar == 'I')
                            {
                                result += 4;
                                i--;
                                break;
                            }
                            result += 5;
                            break;
                        }
                    case 'X':
                        {
                            if (rChar == 'I')
                            {
                                result += 9;
                                i--;
                                break;
                            }
                            result += 10;
                            break;
                        }
                    case 'L':
                        {
                            if (rChar == 'X')
                            {
                                result += 40;
                                i--;
                                break;
                            }
                            result += 50;
                            break;
                        }
                    case 'C':
                        {
                            if (rChar == 'X')
                            {
                                result += 90;
                                i--;
                                break;
                            }
                            result += 100;
                            break;
                        }
                    case 'D':
                        {
                            if (rChar == 'C')
                            {
                                result += 400;
                                i--;
                                break;

                            }
                            result += 500;
                            break;
                        }
                    case 'M':
                        {
                            if (rChar == 'C')
                            {
                                result += 900;
                                i--;
                                break;
                            }
                            result += 1000;
                            break;
                        }
                }
                }
            for (int n = 0; n < arr.Length; n++)
            {
                if (arr[n] == 'V' || arr[n] == 'X' && arr[n-1] == 'I')
                {
                        result -= 2;
                }
                if (arr[n] == 'L' || arr[n] == 'C' && arr[n - 1] == 'X')
                {
                        result -= 20;
                }
                if (arr[n] == 'D' || arr[n] == 'M' && arr[n - 1] == 'C')
                {
                        result -= 200;
                }
            }
            return result;
        }
    }
}
