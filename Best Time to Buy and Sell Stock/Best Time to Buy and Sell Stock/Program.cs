﻿
int[] price = new int [] { 7, 6, 4, 3, 1 };
int maxProfit = 0;
int temp;

for (int i = 0; i < price.Length -1; i++)
{
    for (int j = i+1; j < price.Length; j++)
    {
        if (price[i] < price[j])
        {
            temp = price[j] - price[i];
            if (temp > maxProfit)
            {
                maxProfit = temp;
            }
        }
    }
    
}
Console.WriteLine($"Maximum profit is: {maxProfit}");


