﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;

class Program
{
    static void Main()
    {
        SqlConnection conn = new SqlConnection("Integrated Security = True");

        conn.Open();
        SqlCommand cmd = new SqlCommand("SELECT * FROM [AdventureWorksLT2019].[SalesLT].[Product]", conn);
        SqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            //do work here
            int columNumber = 1;
            string productId = (string)reader[columNumber];
            string name = (string)reader[++columNumber];

            Console.WriteLine($"{productId} {name}");
        }
        Console.ReadKey();
    }
}
