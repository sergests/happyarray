﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NttFarmework
{
    public class StudentRepository : Repository<Student>, IStudentRepository
    {
        private readonly ISchoolContext _context;
        public StudentRepository(ISchoolContext context) : base(context)
        {
            _context = context;
        }

        public void CreateNewStudent(Student domain)
        {
            _context.SetAdded(domain);
            
            _context.SaveChanges();
            var students = GetAll().ToList();
        }
    }

}
