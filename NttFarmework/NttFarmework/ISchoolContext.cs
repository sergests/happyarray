﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace NttFarmework
{
    public interface ISchoolContext
    {
        DbSet<Student> Students { get; set; }
        //DbSet<StudentAddress> StudentAddresses { get; set; }
        //DbSet<Grade> Grades { get; set; }
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        void SetModified(object entity);
        void SetAdded(object entity);
        void SetDeleted(object entity);
        void SetValue(object currentValue, object newValue);
        int SaveChanges();
    }
}
