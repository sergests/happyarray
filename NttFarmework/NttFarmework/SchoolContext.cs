﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace NttFarmework
{
    public class SchoolContext : DbContext, ISchoolContext
    {
        public SchoolContext() : base("Data Source=DESKTOP-890Q20G;Initial Catalog=AdventureWorksLT2019;Integrated Security=True")
        { }

        public DbSet<Student> Students { get; set; }
        // public DbSet<StudentAddress> StudentAddresses { get; set; }
        // public DbSet<Grade> Grades { get; set; }
        public static SchoolContext Create()
        {
            return new SchoolContext();
        }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public virtual void SetAdded(object entity)
        {
            Entry(entity).State = EntityState.Added;
        }

        public void SetDeleted(object entity)
        {
            Entry(entity).State = EntityState.Deleted;
        }

        public void SetValue(object currentValue, object newValue)
        {
            Entry(currentValue).CurrentValues.SetValues(newValue);
        }

        public virtual int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
