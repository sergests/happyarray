﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NttFarmework
{
    public interface IStudentRepository : IRepository<Student>
    {
        void CreateNewStudent(Student domain);
    }
}
