﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NttFarmework
{
    public class Student
    {
        internal Grade Grade;

        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public byte[] Photo { get; set; }
        public decimal Height { get; set; }
        public float Weight { get; set; }
        public int StudentAge { get; set; }

    }
}
