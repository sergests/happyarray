﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace NttFarmework
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //using (SchoolContext db = new SchoolContext())
            //{
            //    bool isCreated = db.Database.Exists();
            //    // bool isCreated2 = await db.Database.EnsureCreatedAsync();
            //    if (isCreated) Console.WriteLine("DB was created");
            //    else Console.WriteLine("DB already exist");
            //}
            //using (var ctx = new SchoolContext())
            //{
            //    var stud = new Student()
            //    {
            //        StudentName = "Bill",
            //        Grade = new Grade() { GradeName = "Second", Section = "Low" }
            //    };

            //    ctx.Students.Add(stud);
            //    ctx.SaveChanges();

            //    var students = ctx.Students.Include("Grade").ToList();

            //    foreach (var student in students)
            //    {
            //        Console.WriteLine($"Student name: {student.StudentName}, Grade = {student.Grade.GradeName}, Grade section = {student.Grade.Section}");
            //    }
            //}

            var repo = new StudentRepository(new SchoolContext());

            repo.CreateNewStudent(new Student()
            {
                StudentName = "Tome"
            });
        }
    }

}
