﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    internal class Program
    {
        
        static void Main(string[] args)
        {

            double firstNum;
            double secondNum;
            double answer;
            string ops;

            Console.WriteLine("\t\t\tHi! This is the Calculator!");
            Console.WriteLine("\t\t\tPlease Enter First Number!");

            firstNum = Double.Parse(Console.ReadLine());

            Console.WriteLine("\t\t\tPlease Select an Operator (+, -, *, /, %)");
            ops = Console.ReadLine();

            Console.WriteLine("\t\t\tPlease Enter Second Number");
            
            secondNum = Double.Parse(Console.ReadLine());

            
            switch (ops)
            {
                case "+": 
                    answer = firstNum + secondNum;
                Console.WriteLine("The result is: " + answer);
                    break;
            
                case "-":
                    answer = secondNum - firstNum;
                Console.WriteLine("The result is: " + answer);
                    break;

                case "*":
                    answer = firstNum * secondNum;
                    Console.WriteLine("The result is: " + answer);
                    break;

                case "/":
                    answer = firstNum / secondNum;
                    Console.WriteLine("The result is: " + answer);
                    break;

                case "%":
                    answer = firstNum % secondNum;
                    Console.WriteLine(answer);
                    break;

                default : throw new Exception("Please enter valid Operator");
            }

            Console.ReadKey();

            Console.WriteLine(answer);
        }
    }
}
