﻿using Google.Cloud.Translation.V2;
using System.Text;

Console.OutputEncoding = Console.InputEncoding = Encoding.ASCII;

Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", @"key.json");
var client = TranslationClient.Create();
foreach (var language in client.ListLanguages(LanguageCodes.English))
{
    Console.WriteLine($"{language.Code}\t{language.Name}");
}

//Console.Clear();
Console.WriteLine("Please enter text.");
var text = Console.ReadLine();

var detectedLanguage = client.DetectLanguage(text);
Console.WriteLine(detectedLanguage.Language);

Console.WriteLine("Please choose Lenguage to translate");
var languageChoosen = Console.ReadLine();

var translatedText = client.TranslateText(text, languageChoosen).TranslatedText;
Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.WriteLine(translatedText);
Console.ReadLine();
