﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_Remove_Duplicates
{
    public class Remover
    {
        public string ShortString(string str)
        {
            Stack<char> st = new Stack<char>();

            int i = 0;
            
            string entered = "azxxzy";

            while (i < entered.Length)
            {
                if (st.Count == 0 || (st.Count != 0 && entered[i] != st.Peek()))
                {
                    st.Push((entered[i]));
                    i++;
                }
                else
                {
                    if (st.Count != 0)
                        st.Pop();
                    i++;
                }
            }
            return String.Join("", st);
        }
        
    }
}
